from circle2d import Circle2D

def c1():
    xPos = 2
    yPos = 2
    rad = 5.5
    area = (3.14*(rad*rad))
    perimeter = (2 * 3.14 *rad)
    containsPoint = (print(""))
    overlaps = print("")
    circle = Circle2D(xPos, yPos, rad, area, perimeter, containsPoint, overlaps)
    print("Circle 1 area:", circle.get_area())
    print("Circle 1 perimeter:",circle.get_perimeter())
    print("")

def c2():
    xPos = 4
    yPos = 5
    rad = 10.5
    containsPoint = (print(""))
    overlaps = print("")
    area = (3.14*(rad*rad))
    perimeter = (2 * 3.14 *rad)
    circle = Circle2D(xPos, yPos, rad, area, perimeter, containsPoint, overlaps)
    print("Circle 2 area:",circle.get_area())
    print("Circle 2 perimeter:",circle.get_perimeter())
    print("")

def c3():
    xPos = 3
    yPos = 5
    rad = 2.3
    containsPoint = (print(""))
    overlaps = print("")
    area = (3.14*(rad*rad))
    perimeter = (2 * 3.14 *rad)
    circle = Circle2D(xPos, yPos, rad, area, perimeter, containsPoint, overlaps)
    print("Circle 3 area:",circle.get_area())
    print("Circle 3 perimeter:",circle.get_perimeter())
    print("")

c1()
c2()
c3()